package ua.pp.voronin.serhii.tommy;

import ua.pp.voronin.serhii.tommy.model.packaging.Box;

public class BoxesProcessor {
    static int calculateBoxPrice(Box box, int pricePerMeter) {
        int boxSize = box.getSide();
        int boxAreainMillimeterSquared = 6 * boxSize * boxSize ;
        double pricePerMilimeter = pricePerMeter / 1000d / 1000d;
        return (int) Math.ceil(boxAreainMillimeterSquared * pricePerMilimeter);
    }
}
